﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helper
{
    public class MockMailSender : IMailSender
    {
        private readonly ILogging logger;

        public MockMailSender(ILogging logger)
        {
            this.logger = logger;
        } 

        public void Send(string toAddress, string subject)
        {
            logger.Log("Sending mock email");
            Console.WriteLine("Mocking mail to [{0}] with subject [{1}]", toAddress, subject);
        }
    }
}
